Title: Introduction à l'algorithmique
Author: Jeremy Vignelles
Date: 2013-08-24 00:00:00
Tags: Algorithmique, Tri, C++

L'algorithmique est une discipline de résolution de problèmes par de la logique. Les algorithmes sont à la base de tout programme informatique.  
Un algorithme est souvent décrit comme une "recette de cuisine", une procédure à suivre pour obtenir un résultat et c'est ce fonctionnement qui est utilisé dans la plupart des langages de programmation.

Pourtant, beaucoup de développeurs délaissent en grande partie cet aspect de l'informatique car ils utilisent des outils prêts à l'emploi qui font ce qu'ils ont besoin, tout en cachant la complexité de la résolution du problème.
Effectivement, utiliser ce genre d'outil s'apparente à jouer aux LEGO: on assemble des briques de base pour faire des éléments plus complexes, mais construire ces briques de base est tout aussi intéressant et pas si complexe qu'il n'y paraît!
Parfois, vous obtiendrez même de meilleures performances en créant vos propres briques!

Une seule chose à faire : réfléchir. (Selon des études poussées menées par des chercheurs, tout le monde en serait capable :p)

## Les algorithmes sont partout

Imaginez un peu... Vous êtes à SUPINFO, en cours d'algorithmique. C'est une discipline que vous détestez par dessus tout parce que vous ne comprenez jamais rien. On vous demande de trier une liste d'éléments et vous cherchez un algorithme tout fait sur internet("Comme ça, le prof sera content"). Vous interrogez donc un moteur de recherche qui va utiliser *son algorithme* pour trouver ce que vous cherchez.

En rentrant chez vous, vous devez aller faire quelques courses au supermarché du coin. Vous payez par carte bleue et la transaction bancaire se fait grâce à plusieurs algorithmes (*interrogation de la banque*, *sécurisation des échanges grâce à un cryptage* ...).

Vous êtes nouveau dans la région et ne savez pas encore rentrer chez vous. Vous allumez donc votre GPS et programmez le plus court chemin pour rentrer chez vous. Après une *phase de calcul*, il peut vous guider.

Une fois chez vous, exténué par tous ces algorithmes qui vous entourent, vous allumez la télévision pour vous détendre un peu. Un candidat à bout de nerfs hurle pour gagner le jeu du *"Plus ou moins"*. Finalement, la télévision, ça énerve...

Alors vous tentez de faire un puzzle, vous commencez par les coins, puis les bords, puis... oh, attendez...

Énervé, vous envoyez valser un coussin du canapé contre le mur. En retombant, celui-ci fait tomber tous vos précieux albums de Tintin. Vous commencez alors à les remettre sur l'étagère, vous ramassez le premier venu, c'est le numéro 5. Le deuxième est le numéro 4, vous le mettez à sa gauche... GRAAAAAHHHHH, c'est encore un algorithme!

Attendez! ça ne sert à rien de s'énerver, l'algorithmique ce n'est pas si terrible que ça! La preuve en est que vous l'avez votre algorithme de tri :) C'est un tri par insertion.

## Un exemple d'algorithme : Le tri par insertion

Votre algorithme est vraiment très simple n'est-ce pas? Et pourtant c'en est bien un. C'est également le même algorithme que vous allez utiliser pour trier vos cartes quand vous jouez à la belote, au tarot, au président etc etc...

![L'algorithme en images](insertionLivres.png)  
*Une étape de l'algorithme en image, l'insertion d'un livre au bon emplacement*

Cet algorithme est assez intuitif, mais il faut à présent expliquer comment faire à un ordinateur. Pour celà, il nous faut:
* La liste des éléments à trier (= L'amas de livres qui est tombé de l'étagère)
* Une liste qui va contenir les éléments une fois triés (L'étagère)
* Un moyen d'insérer un élément dans la liste finale (Pour insérer un livre sur l'étagère, il faut décaler tous les livres qui ont un numéro supérieur au livre qu'on veut insérer et placer le livre à l'endroit libéré. De la rapidité de cette action dépend la rapidité de l'algorithme.)

Voici donc une transcription en C++ de notre algorithme:

    :::cpp
    #include <list>
    void triParInsertion(const std::list<int>& listeLivres, std::list<int>& etagere)
    { //Prends en paramètre le tableau à trier et sort en deuxième paramètre le tableau trié
        for(std::list<int>::const_iterator itLivre = listeLivres.begin();itLivre!=listeLivres.end();itLivre++)
        { //On parcoure notre amas de livres
            std::list<int>::iterator itEtagere;//Va contenir l'endroit où on va insérer l'élément.
            for(itEtagere = etagere.begin();itEtagere != etagere.end();itEtagere++)
            { //On parcoure un à un les éléments déjà placés sur l'étagère
                if(*itEtagere > *itLivre)
                { //Si on trouve un livre dont le numéro est supérieur au livre qu'on vient de ramasser, on va insérer celà avant
                    break;
                }
            }
            /*Ici, deux cas sont possible.
                -Soit il y a au moins un livre sur l'étagère dont le numéro est supérieur et à ce moment là l'itérateur pointe sur le premier de ces éléments. Le insert va insérer avant cet élément
                -Soit il n'y a pas de livre satisfiant cette condition et itEtagere == etagere.end(), le insert va insérer à la fin
            */
            etagere.insert(itEtagere, *itLivre);
        }
        //A la fin, les éléments seront triés dans le bon ordre
    }

Pour ceux qui n'ont jamais fait de C++ ou qui n'ont pas utilisé de listes, sachez simplement qu'un itérateur sert à parcourir un tableau du début à la fin. Il est également utilisé pour certaines autres opérations, comme par exemple l'appel à la méthode insert pour dire à quel endroit on veut insérer l'élément.

Et voilà, votre algorithme est prêt à être utilisé :)
Malheureusement, il n'est pas toujours possible d'insérer un élément à un endroit donné. Dans certains cas, il faudra faire autrement.

## Un autre exemple d'algorithme : Le tri par sélection
Cette fois-ci, vous avez décidé de vous mettre à faire des perles.
Vous avez à disposition une boite de perles de tailles différentes et un fil.
Vous voulez vous fabriquer un beau collier de perles, de la plus petite à la plus grosse.
Seulement, à la différence de l'étagère, vous ne pouvez pas insérer de perle au milieu.

*Hmmm, comment faire?*

Réfléchissez un peu...

Une idée?

Une solution intuitive consiste à trouver la plus petite perle restante dans la boite et l'ajouter sur le fil.
Recommencer jusqu'à ce que la boite soit vide.

![L'algorithme en images](insertionPerles.png)  
*Une étape de l'algorithme en image, l'insertion de la bonne perle au bout du fil*

Et voici une transcription en C++ de cet algorithme :

    :::cpp
    #include <list>
    void triParSelection(std::list<int>& boite, std::list<int>& collier)
    { //Premier paramètre, la liste d'éléments à trier, deuxième paramètre le collier. Ici, je n'ai pas mis le premier paramètre constant car je vais retirer les éléments de la boite une fois qu'ils sont mis dans le collier
        while(!boite.empty())
        { //Tant que la boite n'est pas vide.
            std::list<int>::iterator itMin = boite.begin();
            for(std::list<int>::iterator it=boite.begin();it!=boite.end();it++)
            { //On cherche l'élément minimum
                if(*it < *itMin)
                { //Si on a trouvé un élément plus petit que le plus petit
                    itMin = it;
                }
            } //À noter, cette boucle aurait pu se remplacer par std::list<int> itMin = std::min_element(boite.begin(), boite.end());
            collier.push_back(*itMin); //On ajoute la valeur au collier
            boite.erase(itMin); 	   //On retire l'élément de la boite
        }
    }

TADADADAAAAAAAM :) Voilà un beau collier tout prêt :p

## Encore un exemple : L'exploration de labyrinthe

Fier de votre collier, vous décidez d'aller frimer dehors et organiser une sortie entre amis dans le labyrithe de votre région.
Malheureusement, vos compétences en orientation laissent à désirer et vous cherchez un moyen d'être sûr de trouver la sortie facilement.

Des solutions existent comme par exemple se déplacer "au hasard" ou "à l'intuition" dans le labyrinthe.
Ces deux approches sont similaires mais la deuxième introduit une part de jugement humain qu'il n'est pas facile de transcrire sur un ordinateur.
Malheureusement, ces solutions ne sont pas certaines de donner un résultat dans un temps raisonnable (rien n'empêche de tourner en rond).
Il faut donc trouver autre chose de plus sûr.

![Un exemple de labyrinthe](http://upload.wikimedia.org/wikipedia/commons/8/88/Maze_simple.svg)  
*Source : [Wikipédia](http://commons.wikimedia.org/wiki/File:Maze_simple.svg)*

Une autre solution est possible, car nous avons un labyrinthe fermé.
Il suffit de longer le mur à gauche dès l'entrée (ou à droite, le résultat est le même).
Vous aurez à un moment (plus ou moins long, certes) atteint la sortie, c'est l'algorithme de la main gauche (ou main droite selon la préférence).
Disposant en tant qu'humain d'une mémoire assez limitée qui peut être facilement trompée (rien ne ressemble plus au carrefour d'un labyrinthe qu'un autre carrefour, ils le font exprès en plus!), c'est la solution que l'on va privilégier dans la vie réelle.
C'est une solution fiable qui ne nécessite pas ou peu de réflexion.

Cet algorithme qui nous paraît simple, exprimé en tant qu'humain, est pourtant plus difficile à transcrire dans le langage de l'ordinateur
Étant donné que l'ordinateur dispose d'une puissance de calcul et d'une mémoire plus importante qu'un être humain, il va devenir totalement acceptable d'explorer le labyrithe.

Un ordinateur va pouvoir explorer chaque possibilité à chaque intersection du labyrinthe. Si l'exploration n'aboutit pas (cul-de-sac), on reprend alors la recherche à partir du dernier croisement.

Voici la liste de ce qu'il nous faut:
* Un tableau à deux dimensions qui va représenter notre labyrinthe.
Un # représente un mur, un . un passage et X une case explorée.
La case de départ devra être la seule case marqué d'un X
* Une liste qui va contenir les cases qui n'ont pas été explorées mais qui sont atteignable depuis l'une des cases explorées.

Voici la transcription du labyrinthe ci-dessus :

    #X###################
    #.....#.....#.......#
    #.###.#.###.#.#####.#
    #.#...#...#...#.#...#
    #.#######.###.#.#.###
    #...#...#...#...#.#.#
    ###.#.###.#.#####.#.#
    #.....#...#...#.....#
    #.#####.#.#####.#####
    #.......#...#.......#
    ###################.#

et un code C++ transcrivant cet algorithme:

    :::cpp
    #include <stack>
    #include <exception>

    class Position
    { //Une petite classe utilitaire juste pour stocker les coordonnées X,Y
    public:
        int X;
        int Y;
        Position(int _X, int _Y):X(_X),Y(_Y)
        {}
    };

    void laby(char** labyrinthe, int largeur, int hauteur)
    {
        Position pos(0,0);//La pos, au format (X,Y)
        bool posDepartTrouvee = false;
        for(pos.Y = 0;pos.Y < hauteur;pos.Y++)
        { //Recherche du X qui représente la case de départ.
            for(pos.X = 0;pos.X < largeur;pos.X++)
            {
                if(labyrinthe[pos.Y][pos.X] == 'X')
                { //On a trouvé la case de départ, on interrompt les boucles
                    posDepartTrouvee = true;
                    break;
                }
            }
            if(posDepartTrouvee)
            {
                break;
            }
        }
        if(!posDepartTrouvee)
        {//Si on n'a pas trouvé la case de départ, on arrête tout.
            throw std::invalid_argument("Le labyrinthe doit avoir une case de depart.");
        }

        std::stack<Position> aParcourir;
        //On utilise ici une stack (pile en français), c'est à dire un conteneur LIFO (Dernier arrivé, premier sorti).

        do
        {
            //On est sur une case, 4 choix s'offrent à nous (4 directions). On va regarder parmis ces 4 lesquels sont possibles et libres et les ajouter dans la pile.
            if(pos.X > 0 && labyrinthe[pos.Y][pos.X-1] == '.')
            { //Si on est pas sur le bord de gauche et que la case est libre(pas un mur et pas déjà parcourue)
                aParcourir.push(Position(pos.X-1, pos.Y));
            }
            if(pos.Y > 0 && labyrinthe[pos.Y-1][pos.X] == '.')
            { //Si on est pas sur le bord du haut et que la case est libre
                aParcourir.push(Position(pos.X, pos.Y-1));
            }
            if(pos.X < largeur-1 && labyrinthe[pos.Y][pos.X+1] == '.')
            { //Si on est pas sur le bord de droite et que la case est libre
                aParcourir.push(Position(pos.X+1, pos.Y));
            }
            if(pos.Y < hauteur-1 && labyrinthe[pos.Y+1][pos.X] == '.')
            { //Si on est pas sur le bord du bas et que la case est libre
                aParcourir.push(Position(pos.X, pos.Y+1));
            }

            if(aParcourir.empty())
            { //On a vraiment tout essayé, pas moyen de sortir du labyrinthe
                throw std::runtime_error("Impossible de trouver la sortie de ce labyrinthe!");
            }

            pos = aParcourir.top();			//On se déplace sur une nouvelle case à explorer
            labyrinthe[pos.Y][pos.X] = 'X';	//On marque qu'on est passé sur cette case.
            aParcourir.pop();				//On enlève l'élément.
        } while(pos.X != 0 && pos.X != largeur-1 && pos.Y != 0 && pos.Y != hauteur-1); //Tant qu'on est pas sur un bord, on est pas sorti
    }

Attention, cet algorithme sort du labyrinthe et marque les cases par lesquelles il est passé. Avec un peu de modifications, on pourrait lui faire écrire le chemin de la sortie, mais ce n'était pas l'objectif ici.

Cet algorithme était un peu plus compliqué que les autres. Prenez le temps de bien le comprendre.
Comment fonctionne-t-il?

![L'algorithme en images](maze_solved.svg)  
*Comparaison de l'algorithme ci-dessus (en vert) et de l'algorithme de la main gauche (en rouge)*

On commence par trouver la case de départ. Une fois celle-ci trouvée, on commence à parcourir le labyrinthe.  
On regarde toutes les cases autour de la case actuelle et pour ces cases là, on regarde si il est possible de s'y déplacer.  
Si c'est possible, on ajoute la case à la liste des cases à parcourir.
Le fait que cette liste soit une pile nous aide à aller de l'avant. On va d'abord essayer les cases qui ont été trouvées en dernier.  
On recommence donc avec la case suivante (celle qui a été ajoutée en dernier, donc depuis la case précédente).

Si on trouve un cul-de-sac, on n'ajoute plus de cases à la pile et on va revenir à une case ajoutée avant(c'est à dire revenir à la dernière intersection qui n'a pas été complètement explorée).

Si il n'y a plus de cases à explorer, cela signifie que le labyrinthe est fermé et qu'il n'y a pas de solution.

## Méfiez-vous des LEGO
Les briques logicielles déjà toutes faites peuvent s'avérer pratiques, mais parfois il vaut mieux "réinventer la roue" plutôt que de vouloir à tout prix réutiliser une fonction.

Prenons un exemple. Suite à votre sortie dans le labyrinthe, vous avez réussi à vendre vos créations à une cliente disons... imposante.
Par conséquent, elle souhaite que vous doubliez la taille de vos collier afin qu'elle vous en achète.
Prévoyant, vous aviez fait des réserves de colliers déjà confectionnés sur ce modèle (du plus petit au plus grand), il vous reste juste à en démonter deux pour en faire un seul.
Maintenant que vous avez la technique de montage du collier, vous vous ditez que le plus simple serait de couper les deux colliers, de rassembler les perles dans une boite, de prendre un nouveau fil et de recommencer.
En effet, ça serait plus simple dans le sens où vous n'avez pas besoin de réfléchir sur un nouvel algorithme, mais cette façon de faire vous prendrait donc deux fois plus de temps que pour faire un collier de taille standard (sans compter le temps que vous aviez passé à faire ces deux colliers.)

Voici l'implémentation en c++ de cette *mauvaise* façon de faire:

    :::cpp
    std::list<int> boite;
    boite.insert(boite.end(), collierA.begin(), collierA.end());
    boite.insert(boite.end(), collierB.begin(), collierB.end());
    std::list<int> doubleCollier;
    triParSelection(boite, doubleCollier);

Intuitivement, on se rend bien compte qu'il va nous falloir beaucoup de temps pour tout refaire.
Même si ce code est pratique et très lisible, il vaut peut-être mieux trouver une autre solution.
On pourrait utiliser le fait que les deux colliers sont déjà bien faits, et donc que les deux listes sont déjà triées.

*Temps de réflexion*

Vous avez trouvé ?

La technique consiste donc à couper les deux colliers au début (là où se trouvent les perles les plus petites) et à comparer la taille de la perle qui se trouve à l'extrêmité de chacun. La plus petite est retirée pour aller sur le nouveau fil, l'autre reste en place.
On recommence ainsi jusqu'à ce qu'il n'y aie plus de perles à retirer.

    :::cpp
    void fusionCollier(std::list<int>& collierA, std::list<int>& collierB, std::list<int>& collierFinal)
    { //Fusionne les deux colliers collierA et collierB dans collierFinal
        while(!collierA.empty() || !collierB.empty())
        { //Tant que les deux colliers ne sont pas tous les deux vides
            if(collierA.empty())
            { //Cas particulier, le collier A est vide, il ne reste plus qu'à rajouter tous les éléments de B à la fin du collier final
                collierFinal.insert(collierFinal.end(), collierB.begin(), collierB.end());
                collierB.clear();
            }
            else if(collierB.empty())
            { //Cas particulier, le collier B est vide, il ne reste plus qu'à rajouter tous les éléments de A à la fin du collier final
                collierFinal.insert(collierFinal.end(), collierA.begin(), collierA.end());
                collierA.clear();
            }
            else
            { //Il reste au moins un élément dans chaque collier
                if(collierA.front() < collierB.front())
                {//La première perle de A est plus petite que la première de B (et donc nécessairement que toutes celles du collier B)
                    collierFinal.push_back(collierA.front());//On ajoute notre perle
                    collierA.pop_front();//On retire la perle du collier A
                }
                else
                { //Même chose si c'est la perle du collier B la plus petite
                    collierFinal.push_back(collierB.front());
                    collierB.pop_front();
                }
            }
        }
    }

Effectivement, ce code est un petit peu plus long à écrire, mais il est énormément plus rapide. Sur de petits échantillons (une à deux fusions sur des colliers de quelques élément), la différence ne se voit pas sur un ordinateur, mais pour peu que les colliers contiennent 1000 perles et s'il faut en produire une grande quantité, le ralentissement va commencer à se faire sentir. (Voir fichier zip en bas de cet article)

## Conclusion

L'algorithmique permet de résoudre pas mal de problèmes qui peuvent se poser.
En réfléchissant un peu, on finit souvent par trouver des bonnes solutions.


Voici quelques liens pour vous entraîner grâce à des exercices:
* [France IOI](http://www.france-ioi.org/algo/chapters.php)
* [Prologin](http://prologin.org/training)

Une applet java vous permettant de visualiser différents algorithmes de tri:
[Visualisation de tris](http://thomas.baudel.name/Visualisation/VisuTri/index.fr.html)
Une visualisation similaire, mais sous forme de gif:
[Visualisation de tris](http://thomas.baudel.name/Visualisation/VisuTri/index.fr.html)

Voici une archive qui contient les codes sources de cet article avec des codes de test :
[Codes(Visual studio 2010+ et Code::Blocks)](codes_algorithmes.zip)

Et pour finir, un excellent ouvrage pour aller plus loin grâce à des explications claires:
[Introduction à l'algorithmique - Thomas Cormen, Charles Leiserson, Ronald Rivest, Clifford Stein](http://mitpress.mit.edu/books/introduction-algorithms)